package io.fof.engine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FactOrFictionEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(FactOrFictionEngineApplication.class, args);
	}
}
